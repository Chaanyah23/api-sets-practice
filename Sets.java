import java.util.*;

public class Sets {

    public void sets() {
        Set<Integer> set1 = new HashSet<>();

        set1.add(88);
        set1.add(20);
        set1.add(15);
        set1.add(1);
        set1.add(6);

        System.out.println(set1);

        Set<Integer> set2 = new HashSet<>();

        set2.add(4);
        set2.add(14);
        set2.add(32);
        set2.add(10);
        set2.add(5);

        // Union
        System.out.println("\n");
        System.out.println("Union:");
        set1.addAll(set2);

        System.out.println(set1);

        Set<Integer> set3 = new HashSet<>();

        set3.add(4);
        set3.add(10);
        set3.add(32);
        set3.add(10);
        set3.add(9);

        // Intersections
        System.out.println("\n");
        System.out.println("Intersection:");
        set3.containsAll(set2);

        System.out.println(set3);

        // Differances
        System.out.println("\n");
        System.out.println("Differances:");

        Set<Integer> set4 = new HashSet<>(set3);

        set4.addAll(set3);
        set4.removeAll(set2);

        System.out.println(set4);
    }

}
