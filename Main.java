import java.util.*;

public class Main {

    public static void main(String[] args) {

        Sets classSet = new Sets();

        classSet.sets();

        List<Integer> list1 = new ArrayList<Integer>(Arrays.asList(1, 5, 6, 7, 8, 10, 54, 14, 20, 65, 47));
        List<Integer> list2 = new ArrayList<Integer>(Arrays.asList());

        for (int i = 0; i < list1.size(); i++) {
            System.out.print(list1.get(i) + " ");
        }

        for (int i = 0; i < list1.size(); i++) {
            list2.add(list1.get(i));
        }

        System.out.println("\n");

        for (int i = 0; i < list1.size(); i++) {
            System.out.print(list2.get(i) + " ");
        }

    }

}
